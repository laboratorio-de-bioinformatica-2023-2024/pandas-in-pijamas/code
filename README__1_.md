### Análise da diversidade de Microbiomas.


## Descrição
Este projeto tem como principal propósito desenvolver um software de fácil utilização voltado para a análise e visualização da diversidade microbiológica. O software possibilitará aos usuários carregar dados de sequenciamento do microbioma em formatos amplamente reconhecidos. Após a importação dos dados, o sistema processará as informações para identificar e classificar as taxas microbianas, visando proporcionar uma compreensão clara e acessível da composição do microbioma.

Além disso, a ferramenta calculará e apresentará visualmente índices de diversidade, como riqueza e uniformidade da comunidade microbiana, por meio de ferramentas de visualização interativas, como gráficos de barras, entre outras. Uma característica crucial do software será sua capacidade de comparar diferentes amostras de microbioma, auxiliando os pesquisadores na identificação de padrões e variações entre diversas condições ou populações.

Por fim, o sistema terá a capacidade de gerar relatórios exportáveis que consolidarão os resultados das análises, contribuindo para a documentação de pesquisas.

O projeto conciste numa ferramenta que permite os utilizadores darem upload de dados de sequenciação, este está dividido em várias etapas, cada uma com a sua expecifica função.

Etapa 1: Importação de Dados

Esta etapa é responsável por importar os dados no formato adequado para análise no Qiime2, dependendo do tipo de arquivo fornecido. Cada opção de tipo de arquivo tem um conjunto diferente de comandos de importação.
Etapa 2: Demultiplexação

A demultiplexação é a etapa em que os dados são separados com base nos códigos de barras ou outras informações de indexação. Isso é necessário para identificar a origem dos dados de cada amostra. Novamente, os comandos variam dependendo do tipo de arquivo.
Etapa 3: Denoising (Controle de Qualidade usando DADA2)

Aqui, os dados são processados para remover ruídos e erros. A técnica utilizada é o algoritmo DADA2, que é amplamente utilizado para denoising em dados de sequenciamento.
Etapa 4: Análise Taxonômica e Classificação

Nesta etapa, um classificador pré-treinado é utilizado para atribuir informações taxonômicas às sequências denoised. O resultado é uma tabela de abundância taxonômica.

Etapa 5: Alinhamento e Construção da Árvore Filogenética

Esta etapa envolve o alinhamento das sequências e a construção de uma árvore filogenética, que é útil para análises de diversidade filogenética.

Etapa 6: Análise de Diversidade

A última etapa envolve análises de diversidade microbiológica usando a árvore filogenética e os dados de abundância taxonômica.

##Autores
Shahd Elrakaybi (202100927)
Anilsan Antunes (201900176)
Pedro Pacheco (202100957)
Francisco Amaral (201900202)

##Referências
Qiime2: https://qiime2.org


