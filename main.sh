
#!/bin/bash

display_program_info() {
echo "
####################################################
      
Welcome to the Microbiome Diversity Analysis Tool!
      
####################################################

This is a user-friendly tool created to provide easy microbiome data analysis using the software Qiime2.

Currently, our program can only accept sequence data with sequence quality information in FASTQ format.

The supported files include:
1- FASTQ data with the EMP Protocol format - Single-end reads
2- FASTQ data with the EMP Protocol format - Paired-end reads
3- Multiplexed FASTQ data with barcodes in sequences - Single-end reads
4- Multiplexed FASTQ data with barcodes in sequences - Paired-end reads
5- FASTQ data in the Casava 1.8 demultiplexed format - Single-end reads
6- FASTQ data in the Casava 1.8 demultiplexed format - Paired-end reads


For more information about the Files required for each data type, run the following command:

./main.sh -h [OPTION]

" 
}

display_args_info() {
echo "
####################################################
      
Welcome to the Microbiome Diversity Analysis Tool!
      
####################################################
"
if [ "$2" == "1" ]; then
echo "
Single-end “Earth Microbiome Project (EMP) protocol” formatted reads should have two fastq.gz files total:

1- One forward.fastq.gz file that contains the single-end reads;
2- One barcodes.fastq.gz file that contains the associated barcode reads.
3- One metadata file with a column of per-sample barcodes for use in FASTQ demultiplexing (Note: name the two columns you want to analyse: 'column1' and 'column2').

Because you are importing multiple files in a directory, the filenames forward.fastq.gz and barcodes.fastq.gz are required.

Arguments required:
arg1 --> 1
arg2 --> path to directory containing the two required files.
arg3 --> path to metadata.tsv file.

"

elif [ "$2" == 2 ]; then
echo "
Paired-end “Earth Microbiome Project (EMP) protocol” formatted reads should have three fastq.gz files total:

1- One forward.fastq.gz file that contains the forward sequence reads,
2- One reverse.fastq.gz file that contains the reverse sequence reads,
3- One barcodes.fastq.gz file that contains the associated barcode reads
4- One metadata file with a column of per-sample barcodes for use in FASTQ demultiplexing (Note: name the two columns you want to analyse: 'column1' and 'column2').

In this format, sequence data is still multiplexed (i.e. you have only one forward and one reverse fastq.gz file containing raw data for all of your samples).

Because you are importing multiple files in a directory, the filenames forward.fastq.gz, reverse.fastq.gz, and barcodes.fastq.gz are required.

Arguments required:
arg1 --> 2
arg2 --> path to directory containing the required files.
arg3 --> path to metadata.tsv file.

"

elif [ "$2" == 3 ]; then
echo "
Users with multiplexed single-ended barcodes in sequence reads should have:

1- One fastq.gz file, containing records from multiple samples,
2- One metadata file with a column of per-sample barcodes for use in FASTQ demultiplexing (Note: name the two columns you want to analyze: 'column1' and 'column2').

Arguments required:
arg1 --> 3
arg2 --> path to the required sequences file.
arg3 --> path to metadata.tsv file.

"

elif [ "$2" == 4 ]; then
echo "
Users with multiplexed paired-end barcodes in sequence reads should have:

1- One forward.fastq.gz file, containing forward reads from multiple samples,
2- One reverse.fastq.gz file, containing reverse reads from the same samples,
3- One metadata file with a column of per-sample barcodes for use in FASTQ demultiplexing (Note: name the two columns you want to analyze: 'column1' and 'column2')

In this format, sequence data is still multiplexed (i.e. you have only one forward and one reverse fastq.gz file containing raw data for all of your samples).

Because you are importing a multi-file directory, the filenames forward.fastq.gz and reverse.fastq.gz are required.

Arguments required:
arg1 --> 4
arg2 --> path to directory containing the required files.
arg3 --> path to metadata.tsv file.

"

elif [ "$2" == 5 ]; then
echo "
In the Casava 1.8 demultiplexed (single-end) format, there is one fastq.gz file for each sample in the study which contains the single-end reads for that sample. The file name includes the sample identifier and should look like L2S357_15_L001_R1_001.fastq.gz. The underscore-separated fields in this file name are:

1. The sample identifier,
2. The barcode sequence or a barcode identifier,
3. The lane number,
4. The direction of the read (i.e. only R1, because these are single-end reads), and
5. The set number.

You should also have one metadata file for Diversity Analysis. (Note: name the two columns you want to analyze: 'column1' and 'column2')

Arguments required:
arg1 --> 5
arg2 --> path to directory containing the required files.
arg3 --> path to metadata.tsv file.

"

elif [ "$2" == 6 ]; then
echo "
In Casava 1.8 demultiplexed (paired-end) format, there are two fastq.gz files for each sample in the study, each containing the forward or reverse reads for that sample. The file name includes the sample identifier. The forward and reverse read file names for a single sample might look like L2S357_15_L001_R1_001.fastq.gz and L2S357_15_L001_R2_001.fastq.gz, respectively. The underscore-separated fields in this file name are:

1. The sample identifier,
2. The barcode sequence or a barcode identifier,
3. The lane number,
4. The direction of the read (i.e. R1 or R2), and
5. The set number.

You should also have one metadata file for Diversity Analysis. (Note: name the two columns you want to analyze: 'column1' and 'column2')

Arguments required:
arg1 --> 6
arg2 --> path to directory containing the required files.
arg3 --> path to metadata.tsv file.

"
else
echo "Error: Invalid Option."
fi
}

########################################
# Check whether the number of arguments is correct or not
if [ "$1" == 1 ] || [ "$1" == 2 ] || [ "$1" == 3 ] || [ "$1" == 4 ] || [ "$1" == 5 ] || [ "$1" == 6 ]; then
   if [ "$#" -ne 3 ]; then
       echo "Error: Wrong number of arguments"
       echo "For more information please run: ./main.sh -h $1"
       exit 1
   fi
   file_type=$1
   input_path=$2
   metadata_path=$3
   
elif [ "$1" == "-h" ]; then
   display_args_info $@
   exit 1
   
else 
   display_program_info
   exit 1
fi

#######################################

# Exit on error
set -e

#######################################
# Step 1: Import Data
# Check file type and import the necessary data file/directory in Qiime2.
echo "Step 1: Importing data..."

if [ $file_type == 1 ]; then

   qiime tools import \
     --type EMPSingleEndSequences \
     --input-path $input_path \
     --output-path emp-single-end-sequences.qza
  
   echo "Import Done!"

elif [ $file_type == 2 ]; then

   qiime tools import \
     --type EMPPairedEndSequences \
     --input-path $input_path \
     --output-path emp-paired-end-sequences.qza

   echo "Import Done!"

elif [ $file_type == 3 ]; then

   qiime tools import \
     --type MultiplexedSingleEndBarcodeInSequence \
     --input-path $input_path \
     --output-path multiplexed-seqs.qza

   echo "Import Done!"

elif [ $file_type == 4 ]; then

   qiime tools import \
     --type MultiplexedPairedEndBarcodeInSequence \
     --input-path $input_path \
     --output-path multiplexed-seqs.qza

   echo "Import Done!"

elif [ $file_type == 5 ]; then

   qiime tools import \
     --type 'SampleData[SequencesWithQuality]' \
     --input-path $input_path \
     --input-format CasavaOneEightSingleLanePerSampleDirFmt \
     --output-path demux.qza
     
   qiime demux summarize \
    --i-data demux.qza \
    --o-visualization demux.qzv

   echo "Import Done!"

elif [ $file_type == 6 ]; then

   qiime tools import \
     --type 'SampleData[PairedEndSequencesWithQuality]' \
     --input-path $input_path \
     --input-format CasavaOneEightSingleLanePerSampleDirFmt \
     --output-path demux.qza
   
   qiime demux summarize \
    --i-data demux.qza \
    --o-visualization demux.qzv
  
   echo "Import Done!"

else
   echo "Invalid Option!"
   display_program_info
fi

#######################################################
# Step 2: Demultiplexing

if [ $file_type == 1 ]; then
echo "Step 2: Demultiplexing data...."
qiime demux emp-single \
  --i-seqs emp-single-end-sequences.qza \
  --m-barcodes-file $metadata_path \
  --m-barcodes-column barcode-sequence \
  --o-per-sample-sequences demux.qza \
  --o-error-correction-details demux-details.qza

echo "Demultiplexing Done!"

elif [ $file_type == 2 ]; then
echo "Step 2: Demultiplexing data...."
qiime demux emp-paired \
  --m-barcodes-file $metadata_path \
  --m-barcodes-column barcode-sequence \
  --p-rev-comp-mapping-barcodes \
  --i-seqs emp-paired-end-sequences.qza \
  --o-per-sample-sequences demux.qza \
  --o-error-correction-details demux-details.qza

echo "Demultiplexing Done!"

elif [ $file_type == 3 ]; then
echo "Step 2: Demultiplexing data...."
# a) Demultiplex the reads
qiime cutadapt demux-single \
  --i-seqs multiplexed-seqs.qza \
  --m-barcodes-file $metadata_path \
  --m-barcodes-column Barcode \
  --p-error-rate 0 \
  --o-per-sample-sequences demux.qza \
  --o-untrimmed-sequences untrimmed.qza \
  --verbose

# b) Trim adapters from demultiplexed reads
qiime cutadapt trim-single \
  --i-demultiplexed-sequences demux.qza \
  --p-front GCTACGGGGGG \
  --p-error-rate 0 \
  --o-trimmed-sequences trimmed-seqs.qza \
  --verbose

# c) Summarize demultiplexed and trimmed reads
qiime demux summarize \
  --i-data trimmed-seqs.qza \
  --o-visualization trimmed-seqs.qzv

qiime tools view trimmed-seqs.qzv

echo "Demultiplexing Done!"

elif [ $file_type == 4 ]; then
echo "Step 2: Demultiplexing data...."
# a) Demultiplex the reads
qiime cutadapt demux-paired \
  --i-seqs multiplexed-seqs.qza \
  --m-barcodes-file $metadata_path \
  --m-barcodes-column Barcode \
  --p-error-rate 0 \
  --o-per-sample-sequences demux.qza \
  --o-untrimmed-sequences untrimmed.qza \
  --verbose

# b) Trim adapters from demultiplexed reads
qiime cutadapt trim-paired \
  --i-demultiplexed-sequences demux.qza \
  --p-front GCTACGGGGGG \
  --p-error-rate 0 \
  --o-trimmed-sequences trimmed-seqs.qza \
  --verbose

# c) Summarize demultiplexed and trimmed reads
qiime demux summarize \
  --i-data trimmed-seqs.qza \
  --o-visualization trimmed-seqs.qzv

qiime tools view trimmed-seqs.qzv

echo "Demultiplexing Done!"

elif [ $file_type == 5 ] || [ $file_type == 6 ]; then

echo "Step 2: Demultiplexing Skipped! (unnecessary)"

fi

#############################################################
# Step 3: Denoising Data (Quality control using DADA2 method)

echo "Step 3: Denoising data... (Note: This step may take some time)"

if [ $file_type == 1 ] || [ $file_type == 3 ] || [ $file_type == 5 ]; then
qiime dada2 denoise-single \
  --i-demultiplexed-seqs demux.qza \
  --p-trim-left 0 \
  --p-trunc-len 50 \
  --o-representative-sequences rep-seqs.qza \
  --o-table table.qza \
  --o-denoising-stats stats.qza

# Viewing denoising stats  
qiime metadata tabulate \
  --m-input-file stats.qza \
  --o-visualization stats.qzv

# FeatureTable and FeatureData summaries:
qiime feature-table summarize \
  --i-table table.qza \
  --o-visualization table.qzv

qiime feature-table tabulate-seqs \
  --i-data rep-seqs.qza \
  --o-visualization rep-seqs.qzv


elif [ $file_type == 2 ] || [ $file_type == 4 ] || [ $file_type == 6 ]; then
qiime dada2 denoise-paired \
  --i-demultiplexed-seqs demux.qza \
  --p-trim-left-f 0 \
  --p-trim-left-r 0 \
  --p-trunc-len-f 50 \
  --p-trunc-len-r 50 \
  --o-table table.qza \
  --o-representative-sequences rep-seqs.qza \
  --o-denoising-stats denoising-stats.qza

# FeatureTable and FeatureData summaries:
qiime feature-table summarize \
  --i-table table.qza \
  --o-visualization table.qzv
  --m-sample-metadata-file $metadata_path

qiime feature-table tabulate-seqs \
  --i-data rep-seqs.qza \
  --o-visualization rep-seqs.qzv

qiime metadata tabulate \
  --m-input-file denoising-stats.qza \
  --o-visualization denoising-stats.qzv

fi

echo "Denoising Done!"

#############################################################
# Step 4: Taxonomic analysis and Classification

echo "Step 4: Taxonomic analysis and classification..."

# Get pre-trained classifier available on qiime2 website
wget https://data.qiime2.org/2023.9/common/silva-138-99-nb-classifier.qza

qiime feature-classifier classify-sklearn \
  --i-classifier silva-138-99-nb-classifier.qza \
  --i-reads rep-seqs.qza \
  --o-classification taxonomy.qza

qiime metadata tabulate \
  --m-input-file taxonomy.qza \
  --o-visualization taxonomy.qzv
  
qiime taxa barplot \
  --i-table table.qza \
  --i-taxonomy taxonomy.qza \
  --m-metadata-file $metadata_path \
  --o-visualization taxa-bar-plots.qzv
  
echo "Taxonomic analysis Done!"

#############################################################
# Step 5: Alignment and Phylogenetic Tree

echo "Step 5: Aligning sequences and generating phylogeny..."

# a) Aligning using MAFFT:
qiime alignment mafft \
  --i-sequences rep-seqs.qza \
  --o-alignment aligned-rep-seqs.qza

# b) Mask alignment:
qiime alignment mask \
  --i-alignment aligned-rep-seqs.qza \
  --o-masked-alignment masked-aligned-rep-seqs.qza
  
echo "Alignment Done!"

# c) Construct a phylogeny
qiime phylogeny fasttree \
  --i-alignment masked-aligned-rep-seqs.qza \
  --o-tree tree.qza --verbose

# d) Root the phylogeny
qiime phylogeny midpoint-root \
  --i-tree tree.qza \
  --o-rooted-tree tree-rooted.qza
  
echo "Phylogeny Done!"

#############################################################
# Step 6: Diversity Analysis

echo "Step 6: Analizing Diversity..."

# a) Diversity core metrics
qiime diversity core-metrics-phylogenetic \
  --i-phylogeny tree-rooted.qza \
  --i-table table.qza \
  --p-sampling-depth 1500 \
  --m-metadata-file $metadata_path \
  --output-dir core-metrics-results

# b) Alpha diversity
qiime diversity alpha-group-significance \
  --i-alpha-diversity core-metrics-results/faith_pd_vector.qza \
  --m-metadata-file $metadata_path \
  --o-visualization core-metrics-results/faith-pd-group-significance.qzv

qiime diversity alpha-group-significance \
  --i-alpha-diversity core-metrics-results/evenness_vector.qza \
  --m-metadata-file $metadata_path \
  --o-visualization core-metrics-results/evenness-group-significance.qzv

echo "Alpha Deversity analysis Done!"

# c) Beta diversity
qiime diversity beta-group-significance \
  --i-distance-matrix core-metrics-results/unweighted_unifrac_distance_matrix.qza \
  --m-metadata-file $metadata_path \
  --m-metadata-column column1 \
  --o-visualization core-metrics-results/unweighted-unifrac-column1-significance.qzv \
  --p-pairwise

qiime diversity beta-group-significance \
  --i-distance-matrix core-metrics-results/unweighted_unifrac_distance_matrix.qza \
  --m-metadata-file $metadata_path \
  --m-metadata-column column2 \
  --o-visualization core-metrics-results/unweighted-unifrac-column2-significance.qzv \
  --p-pairwise

echo "Beta Diversity analysis Done!"

#############################################################
# Step 7: Display Results

mkdir -p Qiime2_Artifacts
mkdir -p Qiime2_Visualizations

mv *.qza Qiime2_Artifacts
mv *.qzv Qiime2_Visualizations

echo "
Step 1: Import Data               ==> Done!
Step 2: Demultiplex Data          ==> Done!
Step 3: Denoise Data              ==> Done!
Step 4: Taxonomic analysis        ==> Done!
Step 5: Alignment and phylogeny   ==> Done!
Step 6: Diversity analysis        ==> Done!

Your Microbiome data has been analyzed sucessfully! :)

Artifacts (.qza) have been saved in the Directory 'Qiime2_Artifacts'
Vizualizations (.qzv) have been saved in the Directory 'Qiime2_Vizualisations'
Diversity Analysis results have been saved in the Directory 'core-metrics-results'

To vizualize qiime2 files (.qza/.qzv): https://view.qiime2.org/

############################################################

Thanks for using our Microbiome Diversity Analysis Tool!

############################################################

"

#############################################################
# The End!

